package es.vidal.exerciciudp;

import java.io.IOException;
import java.net.*;

public class UdpServer {

    private static final int PORT = 1234;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {
        try {
            System.out.println("CREATING DATAGRAM SOCKET ON " + MACHINE + " Port: " + PORT);
            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE, PORT);
            DatagramSocket datagramSocket = new DatagramSocket(sockAddr);

            System.out.println("WAITING FOR A MESSAGE ...");
            byte[] message = new byte[40];
            DatagramPacket datagramPacket = new DatagramPacket(message, message.length);

            datagramSocket.receive(datagramPacket);

            InetAddress senderAddr = datagramPacket.getAddress();
            int senderPort = datagramPacket.getPort();

            System.out.println(".... MESSAGE FROM [ "
                    + senderAddr.getHostAddress() + ","
                    + senderPort + " ] RECEIVED: "
                    + new String(message));

            System.out.println(".... SENDING RESPONSE TO SENDER...");
            String s = ">>>>Message received OK<<<<";
            byte[] messageBack = s.getBytes();
            DatagramPacket datagramPacketBack = new DatagramPacket(messageBack, messageBack.length,
                    senderAddr, senderPort);

            datagramSocket.send(datagramPacketBack);

            System.out.println("RESPONSE SENT. CLOSING SOCKET");
            datagramSocket.close();
            System.out.println("SERVER FINISHED");

        } catch (SocketException ex) {
            System.out.println("nSOCKET ERROR: " + ex.getMessage());
        } catch (IOException ex){
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
