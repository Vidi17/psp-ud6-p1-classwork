package es.vidal.exerciciudp;

import java.io.IOException;
import java.net.*;

public class UdpClient {

    private static final int TOPORT = 1234;
    private static final String TOMACHINE = "localhost";

    public static void main(String[] args) {
        try {
            System.out.println("CREATING DATAGRAM SOCKET");

            DatagramSocket datagramSocket = new DatagramSocket();

            System.out.println(".... SENDING MESSAGE...");
            String message = ">>>> Hello I am an UDP CLIENT <<<<";
            DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(),
                    message.getBytes().length,
                    InetAddress.getByName(TOMACHINE), TOPORT);

            datagramSocket.send(datagramPacket);
            System.out.println(".... MESSAGE SENT.");

            System.out.println(".... WAITING FOR RESPONSE...");
            byte[] response = new byte[40];
            datagramPacket = new DatagramPacket(response, response.length);
            datagramSocket.receive(datagramPacket);

            System.out.println(".... MESSAGE RECEIVED: " + new String(response));

            System.out.println("CLOSING SOCKET");
            datagramSocket.close();

            System.out.println("CLIENT FINISHED");

        }catch (SocketException ex) {
            System.out.println("nSOCKET ERROR: " + ex.getMessage());
        }catch (IOException ex) {
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
