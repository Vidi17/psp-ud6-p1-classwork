package es.vidal.probaMultiCast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;

public class MulticastReceiver {

    private static final int PORT_TO_LISTEN = 4444;
    private static final String FROM_MULTICAST_LISTEN = "230.0.0.1";

    public static void main(String[] args) {

        InetSocketAddress inetSocketAddress = new InetSocketAddress(FROM_MULTICAST_LISTEN, PORT_TO_LISTEN);

        try (MulticastSocket s = new MulticastSocket(inetSocketAddress.getPort())){

            s.joinGroup(inetSocketAddress.getAddress());

            byte[] dato = new byte[1000];

            DatagramPacket dgp = new DatagramPacket(dato, dato.length);

            s.receive(dgp);

            System.out.println("Received from UDP: " + new String(dgp.getData()));
        }catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
