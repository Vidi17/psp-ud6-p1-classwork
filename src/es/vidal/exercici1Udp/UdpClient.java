package es.vidal.exercici1Udp;

import java.io.IOException;
import java.net.*;

public class UdpClient {

    private static final int TOPORT = 9090;
    private static final String TOMACHINE = "localhost";

    public static void main(String[] args) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();

            String message = ">>>> Hello I am an UDP CLIENT <<<<";
            DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(),
                    message.getBytes().length,
                    InetAddress.getByName(TOMACHINE), TOPORT);

            datagramSocket.send(datagramPacket);

            byte[] response = new byte[80];
            datagramPacket = new DatagramPacket(response, response.length);
            datagramSocket.receive(datagramPacket);

            System.out.println(new String(response));

            datagramSocket.close();

            while (true) {}


        }catch (SocketException ex) {
            System.out.println("nSOCKET ERROR: " + ex.getMessage());
        }catch (IOException ex) {
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
