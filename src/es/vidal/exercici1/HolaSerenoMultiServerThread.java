package es.vidal.exercici1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class HolaSerenoMultiServerThread extends Thread {
    private final Socket socket;

    public HolaSerenoMultiServerThread(Socket socket) {
        super("HolaSerenoMultiServerThread");
        this.socket = socket;
    }

    public void run() {

        try (
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true)
        ) {
            String outputLine;
            HolaSerenoProtocol serenoProtocol = new HolaSerenoProtocol();
            outputLine = serenoProtocol.processInput();
            out.println(outputLine);

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
