package es.vidal.exercici1;

import java.util.Calendar;

public class HolaSerenoProtocol {

    public String processInput() {

        return "BIENVENIDO AL SERVIDOR SERENO, LA FECHA Y LA HORA ACTUALES SON: " + Calendar.getInstance().toInstant();
    }
}
