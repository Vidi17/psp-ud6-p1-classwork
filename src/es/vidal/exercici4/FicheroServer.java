package es.vidal.exercici4;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class FicheroServer {
    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
            System.err.println("Usage: java FicheroServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);
        File destinationFile = new File("src/es/vidal/exercici4/hola_copia.txt");
        if (!destinationFile.exists()) {
            destinationFile.createNewFile();
        }

        try (
                ServerSocket serverSocket = new ServerSocket(portNumber);
                Socket clientSocket = serverSocket.accept();
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
                FileWriter fileWriter = new FileWriter(destinationFile);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)

        ) {

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                bufferedWriter.write(inputLine);
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                    + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}
