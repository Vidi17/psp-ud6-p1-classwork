package es.vidal.exercici4;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

public class FicheroClient {
    public static void main(String[] args) throws IOException {

        if (args.length != 2) {
            System.err.println(
                    "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);
        File initialFile = new File("src/es/vidal/exercici4/hola.txt");

        try (
                Socket ficheroSocket = new Socket(hostName, portNumber);
                PrintWriter out = new PrintWriter(ficheroSocket.getOutputStream(), true);
                FileReader fileReader = new FileReader(initialFile);
                BufferedReader bufferedReader = new BufferedReader(fileReader);
        ) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                out.println(line);
            }

        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                    hostName);
            System.exit(1);
        }
    }
}
