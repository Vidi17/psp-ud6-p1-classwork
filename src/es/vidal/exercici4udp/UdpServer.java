package es.vidal.exercici4udp;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.*;
import java.util.Arrays;

public class UdpServer {

    private static final int PORT = 9090;
    private static final String MACHINE = "localhost";

    public static void main(String[] args) {
        File ficheroDestino = new File("src/es/vidal/exercici4udp/hola_copia.txt");

        try {
            if (!ficheroDestino.exists()){
                ficheroDestino.createNewFile();
            }

            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE, PORT);
            DatagramSocket datagramSocket = new DatagramSocket(sockAddr);

            byte[] message = new byte[418];
            DatagramPacket datagramPacket = new DatagramPacket(message, message.length);
            datagramSocket.receive(datagramPacket);

            try (FileWriter fileWriter = new FileWriter(ficheroDestino);
                 BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)){

                bufferedWriter.write(new String(message));
            }
            datagramSocket.close();

        } catch (SocketException ex) {
            System.out.println("nSOCKET ERROR: " + ex.getMessage());
        } catch (IOException ex){
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
