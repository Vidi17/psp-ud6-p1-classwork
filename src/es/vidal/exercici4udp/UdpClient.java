package es.vidal.exercici4udp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

public class UdpClient {

    private static final int TOPORT = 9090;
    private static final String TOMACHINE = "localhost";

    public static void main(String[] args) {
        File ficheroInicial = new File("src/es/vidal/exercici4udp/hola.txt");

        try (FileReader fileReader = new FileReader(ficheroInicial);
            BufferedReader bufferedReader = new BufferedReader(fileReader)){

            DatagramSocket datagramSocket = new DatagramSocket();

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                DatagramPacket datagramPacket = new DatagramPacket(line.getBytes(),
                        line.getBytes().length,
                        InetAddress.getByName(TOMACHINE), TOPORT);

                datagramSocket.send(datagramPacket);
            }

        }catch (SocketException ex) {
            ex.printStackTrace();
        }catch (IOException ex) {
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
