package es.vidal.exercici3;

import java.io.IOException;
import java.net.*;

public class UdpServer {

    private static final int PORT = 9090;
    private static final String MACHINE = "localhost";
    private static final boolean listening = true;

    public static void main(String[] args) {
        try {
            InetSocketAddress sockAddr = new InetSocketAddress(MACHINE, PORT);
            DatagramSocket datagramSocket = new DatagramSocket(sockAddr);

            byte[] message = new byte[10];
            DatagramPacket datagramPacket = new DatagramPacket(message, message.length);

            while (listening) {
                datagramSocket.receive(datagramPacket);

                InetAddress senderAddr = datagramPacket.getAddress();
                int senderPort = datagramPacket.getPort();

                System.out.println(".... MESSAGE FROM [ "
                        + senderAddr.getHostAddress() + ","
                        + senderPort + " ] RECEIVED: "
                        + new String(message));
                if (new String(message).toLowerCase().contains("hola")){
                    String s = "Hola cliente, como estás?";
                    byte[] messageBack = s.getBytes();
                    DatagramPacket datagramPacketBack = new DatagramPacket(messageBack, messageBack.length,
                            senderAddr, senderPort);

                    datagramSocket.send(datagramPacketBack);
                } else {
                    String s = "Escribe un mensaje: ";
                    byte[] messageBack = s.getBytes();
                    DatagramPacket datagramPacketBack = new DatagramPacket(messageBack, messageBack.length,
                            senderAddr, senderPort);

                    datagramSocket.send(datagramPacketBack);
                }
            }

        } catch (SocketException ex) {
            System.out.println("nSOCKET ERROR: " + ex.getMessage());
        } catch (IOException ex){
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
