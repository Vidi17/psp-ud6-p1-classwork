package es.vidal.exercici3;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Arrays;
import java.util.Scanner;

public class UdpClient {

    private static final int TOPORT = 9090;
    private static final String TOMACHINE = "localhost";
    private static final Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();

            while (true) {
                String message = input.nextLine();
                DatagramPacket datagramPacket = new DatagramPacket(message.getBytes(),
                        message.getBytes().length,
                        InetAddress.getByName(TOMACHINE), TOPORT);

                datagramSocket.send(datagramPacket);

                byte[] response = new byte[30];
                datagramPacket = new DatagramPacket(response, response.length);
                datagramSocket.receive(datagramPacket);

                System.out.println(new String(response));
            }

        }catch (SocketException ex) {
            ex.printStackTrace();
        }catch (IOException ex) {
            System.out.println("nI/O ERROR: " + ex.getMessage());
        }
    }
}
