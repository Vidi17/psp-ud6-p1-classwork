package es.vidal.exercici2;

import java.io.*;
import java.net.Socket;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MayusculasMultiServerThread extends Thread {
    private final Socket socket;
    private static int clientNumber = 0;

    public MayusculasMultiServerThread(Socket socket) {
        super("MayusculasMultiServerThread");
        this.socket = socket;
    }

    public void run() {
        Logger logger = Logger.getLogger("Log");
        int numberOfCapitalizations = 0;

        try (
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()))
        ) {
            String inputLine, outputLine;
            MayusculasProtocol serenoProtocol = new MayusculasProtocol();
            outputLine = serenoProtocol.processInput(null);
            out.println(outputLine);

            while ((inputLine = in.readLine()) != null) {
                outputLine = serenoProtocol.processInput(inputLine);
                numberOfCapitalizations = inputLine.length();
                out.println(outputLine);
                if (outputLine.equals("QUIT"))
                    break;
            }

            try {
                logger.info(clientNumber + " " + socket.getLocalPort() + " " + socket.getInetAddress() + " " + socket.getPort()
                        + " " + numberOfCapitalizations);
                clientNumber++;

            } catch (SecurityException e) {
                e.printStackTrace();
            }

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
