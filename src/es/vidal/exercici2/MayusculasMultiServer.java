package es.vidal.exercici2;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MayusculasMultiServer {
    public static void main(String[] args) throws IOException {

        if (args.length != 1) {
            System.err.println("Usage: java MayusculasMultiServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);
        boolean listening = true;

        Logger logger = Logger.getLogger("Log");
        FileHandler fh;

        try {

            // This block configure the logger with handler and formatter
            fh = new FileHandler("src/es/vidal/exercici2/LogFile.log");
            logger.addHandler(fh);
            logger.setUseParentHandlers(false);
            MyFormatter formatter = new MyFormatter();
            fh.setFormatter(formatter);
            logger.info("Client Number  LocalPort  ClientIP ClientPort NumberOfCapitalizations");


        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ServerSocket serverSocket = new ServerSocket(portNumber)) {
            while (listening) {
                new MayusculasMultiServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + portNumber);
            System.exit(-1);
        }
    }
}
