package es.vidal.exercici2;

import java.util.Locale;

public class MayusculasProtocol {

    private static final int WAITING = 0;
    private static final int SENTWELCOME = 1;

    private int state = WAITING;


    public String processInput(String theInput) {
        String theOutput = null;

        if (state == WAITING) {
            theOutput = "Buenas, introduce una cadena de texto:";
            state = SENTWELCOME;
        } else if (state == SENTWELCOME) {

            if (theInput.equals("QUIT")){
                theOutput = "QUIT";
            } else {
                theOutput = theInput.toUpperCase(Locale.ROOT);
            }
        }
        return theOutput;
    }
}
